# ubuntu1804-dd18Gb

Configuration scripts having generated the Vagrant box https://app.vagrantup.com/GAEV/boxes/ubuntu1804-dd18Gb

This box was built from a server install image of Ubuntu 18.04 ([ISO file](http://cdimage.ubuntu.com/ubuntu/releases/bionic/release/)), and to be used with VirtualBox as provider.

**List of all added apt packages**:

* whois
* curl
* rsync
* cloud-init
* apt
* net-tools

To generate the corresponding Vagrant box, use the command
```
packer build box-config.json
```

As results, a vagrant box will be generated under the builds folder.


**Keywords**: Ubuntu, VirtualBox, Vagrant, Packer

